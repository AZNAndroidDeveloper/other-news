package uz.azn.othernews.presentation.router

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import uz.azn.othernews.R
import uz.azn.othernews.presentation.main.MainFragment
import uz.azn.othernews.presentation.main.MainFragmentDirections

class Router private constructor(private val navController: NavController) {

    fun navigateToDescription(imageUrl: String, description: String,title:String) {
//        Log.d("Router", "navigateToDescription: ${navController.navigate(R.id.action_mainFragment_to_descNewsFragment).toString()}")
//        navController.navigateUp()
        navController.navigate(
            MainFragmentDirections.actionMainFragmentToDescNewsFragment(imageUrl,description,title)
        )
    }



    companion object {
        private var instance: Router? = null

        fun create(fragment: Fragment, navController: NavController): Router {
            if(fragment !is MainFragment) getRouter()

            if (instance == null)
                instance = Router(navController)

            return checkNotNull(instance)
        }

        fun getRouter(): Router {
            if (instance == null) throw error("Routerga qiymat berilmagan!!!")

            return checkNotNull(instance)
        }
    }
}