package uz.azn.othernews.presentation.error

import android.app.Activity
import android.app.AlertDialog
import android.content.Context

class AlertDialogError {
    companion object{
        fun alertDialogError(activity: Activity){
            val builder = AlertDialog.Builder(activity)
                .setTitle("Internet connection")
                .setCancelable(false)
                .setMessage("Please check your connection and try again ...")
                .setPositiveButton("Exit") { dialog, which ->
                    dialog.dismiss()
                    activity?.finish()
                }
                .setNegativeButton("Retry") { dialog, which ->
                    dialog.dismiss()
                    activity?.recreate()
                }
            val dialog = builder.create()
            dialog.show()
        }
    }
}