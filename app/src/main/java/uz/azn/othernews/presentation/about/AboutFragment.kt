package uz.azn.othernews.presentation.about

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import uz.azn.othernews.R
import uz.azn.othernews.databinding.FragmentAboutBinding


class AboutFragment : Fragment(R.layout.fragment_about), Toolbar.OnMenuItemClickListener {
    private lateinit var binding: FragmentAboutBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentAboutBinding.bind(view)
        with(binding) {
            toolbar.apply {
                setNavigationOnClickListener {
                    findNavController().popBackStack()
                }
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        TODO("Not yet implemented")
    }
}