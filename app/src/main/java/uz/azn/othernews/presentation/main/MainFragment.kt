package uz.azn.othernews.presentation.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import uz.azn.othernews.R
import uz.azn.othernews.databinding.FragmentMainBinding
import uz.azn.othernews.presentation.adapter.AdapterFragment
import uz.azn.othernews.presentation.business.BusinessFragment
import uz.azn.othernews.presentation.darkMode.PreferencesManager
import uz.azn.othernews.presentation.popularity.PopularityFragment
import uz.azn.othernews.presentation.router.Router
import uz.azn.othernews.presentation.sports.SportsFragment

class MainFragment : Fragment(R.layout.fragment_main), Toolbar.OnMenuItemClickListener,
    androidx.appcompat.widget.Toolbar.OnMenuItemClickListener {
    private lateinit var binding: FragmentMainBinding
    private lateinit var preferencesManager: PreferencesManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)
        preferencesManager = PreferencesManager(requireContext())
        val adapterFragment = AdapterFragment(childFragmentManager)
        adapterFragment.setElement("Business", BusinessFragment())
        adapterFragment.setElement("Sports", SportsFragment())
        adapterFragment.setElement("Popularity", PopularityFragment())
        with(binding) {
            viewPager.adapter = adapterFragment
            tabLayout.setupWithViewPager(viewPager)
            toolbar.apply {
                inflateMenu(R.menu.setting_and_about_menu)
                setOnMenuItemClickListener(this@MainFragment)
            }
        }

    }


    override fun onMenuItemClick(item: MenuItem?): Boolean = when (item?.itemId) {
        R.id.about -> {
      findNavController().navigate(MainFragmentDirections.actionMainFragmentToAboutFragment())
            true
        }
        R.id.setting -> {
            findNavController().navigate(MainFragmentDirections.actionMainFragmentToSettingFragment())
            true
        }
        else ->
            false
    }


}
