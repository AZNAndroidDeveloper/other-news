package uz.azn.othernews.presentation.business.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.othernews.network.model.article.model.Article
import uz.azn.othernews.databinding.ViewHolderBusinessNewsBinding as ViewHolderBinding

class BusinessAdapter(
    private val onItemClicked: (Article) -> Unit,
    private val onItemShareClicked: (Article) -> Unit
) : RecyclerView.Adapter<BusinessAdapter.BusinessViewHolder>() {
    private val elements = mutableListOf<Article>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusinessViewHolder =
        BusinessViewHolder(
            ViewHolderBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: BusinessViewHolder, position: Int) {
        holder.onBind(elements[position])
    }

    fun setData(newElements: List<Article>) {
        elements.apply { clear(); addAll(newElements) }
        notifyDataSetChanged()
    }

    fun addData(newElements: List<Article>) {
        elements.addAll(newElements)
        notifyDataSetChanged()
    }

    fun clearData() {
        elements.clear()
        notifyDataSetChanged()
    }

    fun getData(): List<Article> =
        elements

    inner class BusinessViewHolder(
        private val binding: ViewHolderBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun onBind(element: Article) {
            with(binding) {

                dateTextView.text = element.publishedAt.subSequence(0,10)
                descTextView.text = element.description
                titleTextView.text = element.title
                imageSimpleDraweeView.setImageURI(element.imageUrl)
                shareImageView.setOnClickListener { onItemShareClicked.invoke(element) }
                itemView.setOnClickListener { onItemClicked.invoke(element) }
            }
        }
    }
}