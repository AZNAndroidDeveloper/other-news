package uz.azn.othernews.presentation.business

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import retrofit2.Call
import retrofit2.Response
import uz.azn.othernews.R
import uz.azn.othernews.databinding.FragmentBusinessBinding
import uz.azn.othernews.network.mapper.map
import uz.azn.othernews.network.model.article.response.ArticleListResponse
import uz.azn.othernews.network.service.RestProviderImp
import uz.azn.othernews.presentation.business.adapter.BusinessAdapter
import uz.azn.othernews.presentation.error.AlertDialogError
import uz.azn.othernews.presentation.main.MainFragment
import uz.azn.othernews.presentation.router.Router
import uz.azn.othernews.presentation.share.Share

class BusinessFragment : Fragment(R.layout.fragment_business) {

    private lateinit var callArticles: Call<ArticleListResponse>
    private val restProviderImp: RestProviderImp by lazy {
        RestProviderImp()
    }
    private lateinit var binding: FragmentBusinessBinding
    private val share  = Share()
    private val businessAdapter by lazy {
        BusinessAdapter(
            onItemClicked = {
                openDescriptions(it.imageUrl,it.description,it.title) },
            onItemShareClicked = {
                share.share(requireActivity(),it.url)

            }
        )
    }

    override fun onDestroyView() {
        if (this::callArticles.isInitialized)
            callArticles.cancel()
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentBusinessBinding.bind(view)
        with(binding) {
            recycler.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = businessAdapter
            }
        }
        getBusinessNews()
    }

 private fun openDescriptions(imageUrl:String, desc:String,title:String) {
        Router.create(MainFragment(),findNavController())
        Router.getRouter().navigateToDescription(imageUrl,desc,title)
    }

    private fun getBusinessNews() {
        callArticles = restProviderImp.restService.getArticlesBusiness()
        callArticles.enqueue(object : retrofit2.Callback<ArticleListResponse> {
            override fun onFailure(call: Call<ArticleListResponse>, t: Throwable) {
//                AlertDialogError.alertDialogError(requireContext())
            }


            override fun onResponse(
                call: Call<ArticleListResponse>,
                response: Response<ArticleListResponse>
            ) {
                if (response.isSuccessful) {
                    response.body()?.articles?.map { it.map() }?.let {

                        Log.d("TAG", "$it")
                        businessAdapter.setData(it)
                    }
                }
            }
        })
    }

}