package uz.azn.othernews.presentation.darkMode

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("CommitPrefEdits")
class PreferencesManager(var context: Context) {
    private val NIGHT_MODE_STATE = "night_mode_state"
     private var pref :SharedPreferences = context.getSharedPreferences("darkMode",0)
    private var editor : SharedPreferences.Editor

    init {
        editor = pref.edit()

    }
// 0 - off 1 - on 2 -system
    fun setNightMode(state:Int) {
    var mode = state
        when (mode) {
            0 -> mode = 1
            1 -> mode = 2
            2 -> mode = 0
        }
        editor.putInt(NIGHT_MODE_STATE, mode)
        editor.commit()
    }

    fun getNightMode(): Int {
        return pref.getInt(NIGHT_MODE_STATE, 2)
    }
   private fun isNightModeEnabled(): Boolean {
        var mode = 2
        mode = pref.getInt(NIGHT_MODE_STATE, mode)
        return mode > 0
    }
}