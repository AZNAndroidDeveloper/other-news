package uz.azn.othernews.presentation.share

import android.app.Activity
import android.content.Intent

class Share {
        fun share(activity: Activity,uri:String){
            val shareIntent  =Intent().apply {
                action = Intent.ACTION_SEND
                type= "text/plain"
//                putExtra(Intent.EXTRA_TEXT,"The latest news is in Other news")
                putExtra(Intent.EXTRA_TEXT, " The latest news is in Other news \n \n $uri")
            }
            activity.startActivity(Intent.createChooser(shareIntent,"Share"))
        }
    }
