package uz.azn.othernews.presentation.setting

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_setting.*
import uz.azn.othernews.R
import uz.azn.othernews.databinding.FragmentSettingBinding
import uz.azn.othernews.presentation.darkMode.PreferencesManager


class SettingFragment : Fragment(R.layout.fragment_setting) {
    private lateinit var binding: FragmentSettingBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val preferencesManager = PreferencesManager(requireContext())
        when {
            preferencesManager.getNightMode() == 1 -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                switch_compat.isChecked =  true
            }
            preferencesManager.getNightMode() == 2 -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                switch_compat.isChecked =  false
            }

            else -> {

                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                switch_compat.isChecked  = true

            }
        }
        binding = FragmentSettingBinding.bind(view)
        with(binding) {
            toolbar.setNavigationOnClickListener {
                findNavController().navigate(R.id.action_settingFragment_to_mainFragment)
            }


          switchCompat.setOnCheckedChangeListener { buttonView, isChecked ->
              if (isChecked){
                 preferencesManager.setNightMode(0)
              activity?.recreate()
              }
              else{
                  preferencesManager.setNightMode(1)
                  activity?.recreate()
              }
              }


          }

        }
    }


