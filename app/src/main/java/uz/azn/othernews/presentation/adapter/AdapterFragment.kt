package uz.azn.othernews.presentation.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class AdapterFragment(fragmentManager: FragmentManager):FragmentPagerAdapter(fragmentManager){
    private val fragmentList: MutableList<Fragment> = arrayListOf()
    private  var element:MutableList<String> = arrayListOf()
    override fun getItem(position: Int): Fragment  = fragmentList[position]

    override fun getCount(): Int  = element.size

     fun setElement(element:String, fragment: Fragment){
     this.fragmentList.add(fragment)
        this.element.add(element)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return element[position]
    }
}