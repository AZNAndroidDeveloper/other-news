package uz.azn.othernews.presentation.sports

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import retrofit2.Call
import retrofit2.Response
import uz.azn.othernews.R
import uz.azn.othernews.databinding.FragmentSportsBinding
import uz.azn.othernews.network.mapper.map
import uz.azn.othernews.network.model.article.response.ArticleListResponse
import uz.azn.othernews.network.service.RestProviderImp
import uz.azn.othernews.presentation.error.AlertDialogError
import uz.azn.othernews.presentation.main.MainFragment
import uz.azn.othernews.presentation.router.Router
import uz.azn.othernews.presentation.share.Share
import uz.azn.othernews.presentation.sports.adapter.SportsAdapter

class SportsFragment : Fragment(R.layout.fragment_sports) {

    private lateinit var callArticles: Call<ArticleListResponse>
    private val share = Share()
    private val restProviderImp: RestProviderImp by lazy {
        RestProviderImp()
    }
    private val sportsAdapter by lazy {
        SportsAdapter(onItemClicked = {
            openDescriptions(it.imageUrl,it.description,it.title)
        },
            onItemShareClicked = {
                share.share(requireActivity(),it.url)
            })

    }
    private lateinit var binding: FragmentSportsBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSportsBinding.bind(view)
        with(binding) {
            recycler.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = sportsAdapter
            }
        }
        getSportsNews()
    }

    private fun getSportsNews() {
        callArticles = restProviderImp.restService.getArticlesSport()
        callArticles.enqueue(object : retrofit2.Callback<ArticleListResponse> {
            override fun onFailure(call: Call<ArticleListResponse>, t: Throwable) {
                AlertDialogError.alertDialogError(requireActivity())

            }

            override fun onResponse(
                call: Call<ArticleListResponse>,
                response: Response<ArticleListResponse>
            ) {
                if (response.isSuccessful) {
                    response.body()?.articles?.map { it.map() }?.let {
                        sportsAdapter.setData(it)
                    }
                }
            }

        })
    }
    private fun openDescriptions(imageUrl:String, desc:String,title:String) {
        Router.create(MainFragment(),findNavController())
        Router.getRouter().navigateToDescription(imageUrl,desc,title)
    }

}