package uz.azn.othernews.presentation.descNews

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import uz.azn.othernews.R
import uz.azn.othernews.databinding.FragmentDescNewsBinding

class DescNewsFragment : Fragment(R.layout.fragment_desc_news) {
    private lateinit var binding:FragmentDescNewsBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDescNewsBinding.bind(view)
        with(binding){
            descTextView.text = fragmentArgs.description
            imageSimpleDrawerView.setImageURI(fragmentArgs.imageUrl)
            titleTextView.text =  fragmentArgs.title
        }

    }
    private val fragmentArgs by lazy {
        navArgs<DescNewsFragmentArgs>().value
    }

}