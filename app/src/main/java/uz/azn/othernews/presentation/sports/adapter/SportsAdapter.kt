package uz.azn.othernews.presentation.sports.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.othernews.network.model.article.model.Article
import uz.azn.othernews.databinding.ViewHolderSportsNewsBinding as SportsBinding

class SportsAdapter(
    private val onItemClicked: (Article) -> Unit,
    private val onItemShareClicked: (Article) -> Unit
) : RecyclerView.Adapter<SportsAdapter.SportsViewHolder>() {

    private val elements = mutableListOf<Article>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SportsViewHolder =
        SportsViewHolder(
            SportsBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: SportsViewHolder, position: Int) {

        holder.onBind( elements[position])
    }

    inner class SportsViewHolder(private val binding: SportsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(element: Article) {
            with(binding) {
                cardView.apply {
                    setOnClickListener { onItemClicked.invoke(element) }
                }
                descTextView.text = element.description
                imageSimpleDraweeView.setImageURI(element.imageUrl)
                dateTextView.text = element.publishedAt.subSequence(0,10)
                titleTextView.text = element.title
                shareImageView.setOnClickListener {
                    onItemShareClicked.invoke(element)
                }
            }
        }

    }

    fun setData(newElements: List<Article>) {
        elements.apply { clear(); addAll(newElements) }
        notifyDataSetChanged()
    }

    fun addData(newElements: List<Article>) {
        elements.addAll(newElements)
        notifyDataSetChanged()
    }

    fun clearData() {
        elements.clear()
        notifyDataSetChanged()
    }

    fun getData(): List<Article> =
        elements
}
