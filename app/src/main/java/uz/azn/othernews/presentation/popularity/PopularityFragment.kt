package uz.azn.othernews.presentation.popularity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import okhttp3.internal.addHeaderLenient
import retrofit2.Call
import retrofit2.Response
import uz.azn.othernews.R
import uz.azn.othernews.databinding.FragmentPopularityBinding
import uz.azn.othernews.network.mapper.map
import uz.azn.othernews.network.model.article.response.ArticleListResponse
import uz.azn.othernews.network.service.RestProviderImp
import uz.azn.othernews.presentation.error.AlertDialogError
import uz.azn.othernews.presentation.main.MainFragment
import uz.azn.othernews.presentation.margindecoration.MarginDecoration
import uz.azn.othernews.presentation.popularity.adapter.PopularityAdapter
import uz.azn.othernews.presentation.router.Router
import uz.azn.othernews.presentation.share.Share

class PopularityFragment : Fragment(R.layout.fragment_popularity){
    // val args:PopularityArgs by args()

    private lateinit var binding: FragmentPopularityBinding
    private lateinit var callArticles: Call<ArticleListResponse>
    private val restProviderImp: RestProviderImp by lazy {
        RestProviderImp()
    }
    private val share = Share()
    private val popularityAdapter by lazy {
        PopularityAdapter(onItemClicked = {
            openDescriptions(it.imageUrl, it.description, it.title)
        },
            onItemShareClicked = {
                share.share(requireActivity(), it.url)
            })
        //arg.kelgan malumot
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentPopularityBinding.bind(view)

        with(binding) {
           recycler.apply {
               addItemDecoration(MarginDecoration(4))
               layoutManager =GridLayoutManager(requireContext(),2)
                adapter = popularityAdapter
            }

      }
        binding.recycler
        getPopularityNews()
    }


    private fun getPopularityNews() {
        callArticles = restProviderImp.restService.getArticlesPopularity()
        callArticles.enqueue(object : retrofit2.Callback<ArticleListResponse> {
            override fun onFailure(call: Call<ArticleListResponse>, t: Throwable) {
                AlertDialogError.alertDialogError(requireActivity())
            }

            override fun onResponse(
                call: Call<ArticleListResponse>,
                response: Response<ArticleListResponse>
            ) {
                if (response.isSuccessful) {
                    response.body()?.articles?.map { it.map() }?.let {
                        popularityAdapter.setData(it)

                    }

                }
            }

        })
    }

    private fun openDescriptions(imageUrl: String, desc: String, title: String) {
//findNavController().navigate(R.id.action_mainFragment_to_descNewsFragment)
//        Log.d("Popularity Fragment", "openDescriptions: ${Router.create(MainFragment(), findNavController())}")
//        Log.d("Popularity Fragment", "openDescriptions: ${Router.getRouter().navigateToDescription(imageUrl, desc, title).toString()}")
        val findNavController  = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
        Router.create(MainFragment(), findNavController)
        Router.getRouter().navigateToDescription(imageUrl, desc, title)
    }
}