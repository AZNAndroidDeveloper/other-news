package uz.azn.othernews.presentation.margindecoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MarginDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {

        val itemPosition = parent.getChildAdapterPosition(view)
        if (itemPosition == RecyclerView.NO_POSITION) {
            return
        }
        val itemCount = state.itemCount
        with(outRect) {
            if (itemPosition%2==0) {

                top = spaceHeight
                left = -10
                right = -18
                bottom = spaceHeight
            }
            else  {
                top = spaceHeight
                left = -18
                right = -10
                bottom = spaceHeight
            }

        }
    }
}