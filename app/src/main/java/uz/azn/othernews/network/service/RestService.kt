package uz.azn.othernews.network.service

import retrofit2.Call
import retrofit2.http.GET
import uz.azn.othernews.network.constans.ConstantsApiPathBusiness
import uz.azn.othernews.network.constans.ConstantsApiPathPopularity
import uz.azn.othernews.network.constans.ConstantsApiPathSports
import uz.azn.othernews.network.model.article.response.ArticleListResponse

internal interface RestService {
    @GET("${API_TOP_HEADINGS_BUSINESS}${ConstantsApiPathBusiness.API_API_KEY}")
    fun getArticlesBusiness(): Call<ArticleListResponse>

    @GET("${API_TOP_HEADINGS_SPORTS}${ConstantsApiPathSports.API_API_KEY}")
    fun getArticlesSport(): Call<ArticleListResponse>

    @GET("${API_ARTICLES}${ConstantsApiPathPopularity.API_API_KEY}")
    fun getArticlesPopularity(): Call<ArticleListResponse>

    private companion object {
        const val API_TOP_HEADINGS_BUSINESS: String = "top-headlines"
        const val API_ARTICLES: String = "everything"
        const val API_TOP_HEADINGS_SPORTS: String = "top-headlines"
    }

}