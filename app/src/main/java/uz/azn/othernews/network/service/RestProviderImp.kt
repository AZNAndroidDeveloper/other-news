package uz.azn.othernews.network.service

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uz.azn.othernews.network.service.holder.RestServicesHolder

internal class RestProviderImp() : RestProvider {
    private val restServicesHolder: RestServicesHolder = RestServicesHolder()

    // loginInterceptor yani bu internetdan malumot olib kelib turgani korsatadi logda
    private val httpLoginInterceptor = HttpLoggingInterceptor()
    private val okHttpClient: OkHttpClient by lazy {
        httpLoginInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return@lazy OkHttpClient.Builder()
            .addInterceptor(httpLoginInterceptor)
            .build()
    }

    private val retrofit: Retrofit by lazy {
        return@lazy Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }
    override val restService: RestService =
        retrofit.create(RestService::class.java).also { restServicesHolder.restService = it }

    companion object {
        const val API_BASE_URL: String = "http://newsapi.org/v2/"
    }
}
