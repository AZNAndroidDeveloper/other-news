package uz.azn.othernews.network.mapper

import uz.azn.othernews.network.model.article.model.Article
import uz.azn.othernews.network.model.article.response.ArticleResponse

internal fun ArticleResponse.map(): Article =
    Article(
        author = author ?: "",
        content = content ?: "",
        description = description ?: "",
        publishedAt = publishedAt ?: "",
        sourceId = source?.id,
        sourceName = source?.name,
        isHasSource = source?.name != null,
        title = title,
        url = url ?: "",
        imageUrl = urlToImage ?: ""
    )