package uz.azn.othernews.network.model.article.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ArticleResponse(

    @SerialName("outher")
    val author: String?,

    @SerialName("content")
    val content: String?,

    @SerialName("description")
    val description: String?,

    @SerialName("publishedAt")
    val publishedAt: String?,

    @SerialName("source")
    val source: SourceResponse?,

    @SerialName("title")
    val title: String,

    @SerialName("url")
    val url: String?,

    @SerialName("urlToImage")
    val urlToImage: String?
)