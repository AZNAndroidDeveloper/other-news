package uz.azn.othernews.network.model.article.model

data class Article(
    val author: String,
    val content: String,
    val description: String,
    val publishedAt: String,
    val sourceId: String?,
    val sourceName: String?,
    val isHasSource: Boolean,
    val title: String,
    val url: String,
    val imageUrl: String,
    val likedCount: Long = 0,
    val isLiked: Boolean = false
) : java.io.Serializable {

    val likeTotalCount: Long
        get() = likedCount + if (isLiked) 1 else 0
}
